/*
  RFID RDM6300 data decoder library
 (c) Stephane Driussi 20150623
 Not for commercial use
 Refer to rdm6300_decoder_wiring.jpg diagram for proper connection
 */

#include <SoftwareSerial.h>
#include <RDM6300.h>
SoftwareSerial RFID(2, 3); // RX and TX

int creset = 8; //Telling arduino that you are connecting pin 8 with reset
String stringOne = "";
String stringTwo = "";
int Led=13;
uint8_t Payload[6]; // used for read comparisons
boolean RST = false;

RDM6300 RDM6300(Payload);

void setup()
{
  digitalWrite(creset, HIGH);
  pinMode(creset, OUTPUT); 
  pinMode(Led, OUTPUT);
  RFID.begin(9600);    // start serial to RFID reader
  Serial.begin(9600);  // start serial to PC 
  RST = false;
}

void loop()
{
  while (RFID.available() > 0) 
  {
    digitalWrite(Led, HIGH);
    uint8_t c = RFID.read();
    //Serial.print(c,HEX);
    if (RDM6300.decode(c)) {
      for (int i=0; i < 5; i++){
        Serial.print(Payload[i], HEX);
        Serial.print(" ");
      } 
      Serial.println();
      stringOne = (Payload[2]);
      stringTwo = String(Payload[3],HEX);
      stringTwo += String(Payload[4],HEX);
      Serial.print(zeros(stringOne,3));
      Serial.print(",");
      Serial.print(zeros(String(hexToDec(stringTwo)),5));
      Serial.println(); 
      RST = true;
      break;
    } 
  }
  digitalWrite(Led, LOW);   
  delay(100);
  if(RST){
    reset();
  }
}

void reset(){
  digitalWrite(creset, LOW);  // Reset arduino  
}

String hex_to_dec(String str_comp){
  String str_hex;
  String str_dec;
  
  for (long i=0; i< 65535; i++){
    str_hex = String(i,HEX);
    if (str_hex == str_comp){
      str_dec = String(i);
      break;
    }
  }
  return zeros(str_dec,5);
}

String zeros(String str, int limit){
  for (int i=0; i<limit-str.length(); i++){
    str = "0" + str;
  }
  return str;
}

unsigned int hexToDec(String hexString) {
  unsigned int decValue = 0;
  int nextInt;
  
  for (int i = 0; i < hexString.length(); i++) {
    
    nextInt = int(hexString.charAt(i));
    if (nextInt >= 48 && nextInt <= 57) nextInt = map(nextInt, 48, 57, 0, 9);
    if (nextInt >= 65 && nextInt <= 70) nextInt = map(nextInt, 65, 70, 10, 15);
    if (nextInt >= 97 && nextInt <= 102) nextInt = map(nextInt, 97, 102, 10, 15);
    nextInt = constrain(nextInt, 0, 15);
    
    decValue = (decValue * 16) + nextInt;
  }
  return decValue;
}
